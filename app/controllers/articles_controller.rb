class ArticlesController < ApplicationController
  http_basic_authenticate_with name: "dhh", password: "secret", except: [:index, :show]
  
  def new
    @article = Article.new
  end

  def create
    # Version one
    # render plain: params[:article].inspectdd

    # Version two, with security breach
    # @article = Article.new(params[:article])

    # Version three, with less reusable
    # @article = Article.new(params.require(:article).permit(:title, :text))

    # Version four, best solution, can be used for update
    @article = Article.new(article_params)

    if @article.save
      redirect_to @article
    else
      render 'new'
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
  end

  def show
    @article = Article.find(params[:id])
  end

  def index
    @articles = Article.all
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to articles_path
  end


  private

  def article_params
    params.require(:article).permit(:title, :text)
  end
end
